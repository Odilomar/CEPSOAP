var ceps = require("./cep.js");
var bairros = require("./bairro.js");

const soap = require('soap');

const url = 'https://apphom.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl';

// // document.getElementById('text').innerHTML = "";


for (let i = 0; i < ceps.length; i++) {

    ceps[i].cep = ceps[i].cep.replace(".", "").replace("-", "").trim();
    soap.createClient(url, (err, client) => {
        if (err)
            console.log(err);
        else {

            client.consultaCEP({
                cep: ceps[i].cep
            }, (err, res) => {

                if (err) {
                    console.log("UPDATE geral.cep SET cs_ativo = FALSE WHERE id_cep = " + ceps[i].idCep + ";");
                    return;
                }

                if (res.return.end != "") {
                    var strCep = "UPDATE geral.cep SET tx_logradouro = '" + res.return.end + "'";

                    for (let j = 0; j < bairros.length; j++) {
                        if (bairros[j].bairro == res.return.bairro) {
                            strCep += ", id_bairro = " + bairros[j].idBairro;
                            break;
                        }
                    }

                    strCep += " WHERE id_cep = " + ceps[i].idCep + ";";

                    console.log(strCep);
                }
                else
                {
                    console.log("UPDATE geral.cep SET cs_ativo = FALSE WHERE id_cep = " + ceps[i].idCep + ";");         
                }
            });

        }
    });
}

